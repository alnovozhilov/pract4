#include "twointen.h"
#include "tenintwo.h"
#include <QString>

QString TwoInEight(QString val)
{
    val = TwoInTen(val);
    val = TenInEight(val);
    return val;
}

/**
*\file
*\brief Описание функции перевода из 2-ной в 8-ную систему счисления
*\author Alexandr
*\version 1.4
*/
