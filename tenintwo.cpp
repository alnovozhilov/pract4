#include <QString>

QString TenInTwo(QString val)
{
    int value = val.toInt();
    QString str;

    while (value != 0)
    {
        if (value % 2 == 0)
        {
            str = str+"0";
        }
        else
        {
            str = str+"1";
        }
        value = value / 2;
    }
    val = str;
    for (int i = str.size()-1, j=0; i >= 0; i--, j++)
    {
        val[j] = str[i];
    }
    return val;
}

QString TenInEight(QString val)
{
    int value = val.toInt();
    QString str;
    QString str1;
    while (value !=0)
    {
        str1.setNum(value % 8);
        str = str+str1;
        value = value / 8;
    }
    val = str;
    for (int i = str.size()-1, j=0; i >= 0; i--, j++)
    {
        val[j] = str[i];
    }
    return val;
}

QString TenInSixteen(QString val)
{
    int value = val.toInt();
    QString str;
    QString str1;
    while (value !=0)
    {
        if (value % 16 > 9)
        {
            if (value % 16 == 10){
                str1 = "A";
            }
            if (value % 16 == 11){
                str1 = "B";
            }
            if (value % 16 == 12){
                str1 = "C";
            }
            if (value % 16 == 13){
                str1 = "D";
            }
            if (value % 16 == 14){
                str1 = "E";
            }
            if (value % 16 == 15){
                str1 = "F";
            }
        }
        else
        {
            str1.setNum(value % 16);
        }
        str = str+str1;
        value = value / 16;
    }
    val = str;
    for (int i = str.size()-1, j=0; i >= 0; i--, j++)
    {
        val[j] = str[i];
    }
    return val;
}

/**
*\file
*\brief Описание функций перевода из 10-ной в 2-ную, 8-ную, 16-ную системы счисления
*\author Alexandr
*\version 1.4
*/
