#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tenintwo.h"
#include "twointen.h"
#include "twoineight.h"
#include "eightintwo.h"
#include "eightinsixteen.h"
#include "sixteenineight.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->listView->setModel(model);

    ui->pushButton->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SetEnabled()
{
    ui->pushButton->setEnabled(ui->Value->hasAcceptableInput());
}

void MainWindow::on_pushButton_clicked()
{
    if (ui->twIntn->isChecked())
    {
        ui->Result->setText(TwoInTen(ui->Value->text()));
    }
    if (ui->tnIntw->isChecked())
    {
        ui->Result->setText(TenInTwo(ui->Value->text()));
    }
    if (ui->twInei->isChecked())
    {
        ui->Result->setText(TwoInEight(ui->Value->text()));
    }
    if (ui->eiIntw->isChecked())
    {
        ui->Result->setText(EightInTwo(ui->Value->text()));
    }
    if (ui->eiInsi->isChecked())
    {
        ui->Result->setText(EightInSixteen(ui->Value->text()));
    }
    if (ui->siInei->isChecked())
    {
        ui->Result->setText(SixteenInEight(ui->Value->text()));
    }

    item = new QStandardItem(QString(ui->Result->text()));
    model->appendRow(item);
}

void MainWindow::on_twIntn_clicked()
{
    ui->Value->clear();
    QRegExp BinSyst("[0-1]{1,50}");
    ui->Value->setValidator(new QRegExpValidator(BinSyst, this));
    connect(ui->Value, SIGNAL(textChanged(QString)), this, SLOT(SetEnabled()));
}

void MainWindow::on_twInei_clicked()
{
    ui->Value->clear();
    QRegExp BinSyst("[0-1]{1,50}");
    ui->Value->setValidator(new QRegExpValidator(BinSyst, this));
    connect(ui->Value, SIGNAL(textChanged(QString)), this, SLOT(SetEnabled()));
}

void MainWindow::on_eiIntw_clicked()
{
    ui->Value->clear();
    QRegExp OctSyst("[0-7]{1,50}");
    ui->Value->setValidator(new QRegExpValidator(OctSyst, this));
    connect(ui->Value, SIGNAL(textChanged(QString)), this, SLOT(SetEnabled()));
}

void MainWindow::on_eiInsi_clicked()
{
    ui->Value->clear();
    QRegExp OctSyst("[0-7]{1,50}");
    ui->Value->setValidator(new QRegExpValidator(OctSyst, this));
    connect(ui->Value, SIGNAL(textChanged(QString)), this, SLOT(SetEnabled()));
}

void MainWindow::on_tnIntw_clicked()
{
    ui->Value->clear();
    QRegExp DecSyst("[0-9]{1,50}");
    ui->Value->setValidator(new QRegExpValidator(DecSyst, this));
    connect(ui->Value, SIGNAL(textChanged(QString)), this, SLOT(SetEnabled()));
}

void MainWindow::on_siInei_clicked()
{
    ui->Value->clear();
    QRegExp HexSyst("[0-9A-F]{1,50}");
    ui->Value->setValidator(new QRegExpValidator(HexSyst, this));
    connect(ui->Value, SIGNAL(textChanged(QString)), this, SLOT(SetEnabled()));
}

 /**
*\file
*\brief Описание класса главного окна
*\author Alexandr
*\version 1.4
*/
