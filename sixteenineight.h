#ifndef SIXTEENINEIGHT_H
#define SIXTEENINEIGHT_H
#include <QString>

QString SixteenInEight(QString val);

/**
*\file
*\brief Заголовочный файл функции перевода из 16-ной в 8-ную систему счисления
*\author Alexandr
*\version 1.4
*/

#endif // SIXTEENINEIGHT_H
