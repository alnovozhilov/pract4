#include "tst_pract3_tests.h"
#include <QtTest>
#include <QString>
#include <../eightinsixteen.h>
#include <../eightintwo.h>
#include <../sixteenineight.h>
#include <../tenintwo.h>
#include <../twoineight.h>
#include <../twointen.h>

Pract3_Tests::Pract3_Tests()
{

}

void Pract3_Tests::test_eightinsixteen()
{
    QString test_val = "2741";
    QString result = "5E1";
    QCOMPARE(result, EightInSixteen(test_val));
}

void Pract3_Tests::test_eightintwo()
{
    QString test_val = "723";
    QString result = "111010011";
    QCOMPARE(result, EightInTwo(test_val));
}

void Pract3_Tests::test_sixteenineight()
{
    QString test_val = "FAD";
    QString result = "7655";
    QCOMPARE(result, SixteenInEight(test_val));
}

void Pract3_Tests::test_tenintwo()
{
    QString test_val = "10";
    QString result = "1010";
    QCOMPARE(result, TenInTwo(test_val));
}

void Pract3_Tests::test_twoineight()
{
    QString test_val = "1111";
    QString result = "17";
    QCOMPARE(result, TwoInEight(test_val));
}

void Pract3_Tests::test_twointen()
{
    QString test_val = "1001";
    QString result = "9";
    QCOMPARE(result, TwoInTen(test_val));
}

void Pract3_Tests::test_tenineight()
{
    QString test_val = "100";
    QString result = "144";
    QCOMPARE(result, TenInEight(test_val));
}

void Pract3_Tests::test_teninsixteen()
{
    QString test_val = "1";
    QString result = "1";
    QCOMPARE(result, TenInSixteen(test_val));
}
//#include "tst_pract3_tests.moc"
