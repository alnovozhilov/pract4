#ifndef EIGHTINSIXTEEN_H
#define EIGHTINSIXTEEN_H
#include <QString>

QString EightInSixteen(QString val);

/**
*\file
*\brief Заголовочный файл функции перевода из 8-ной в 16-ную систему счисления
*\author Alexandr
*\version 1.4
*/

#endif // EIGHTINSIXTEEN_H
