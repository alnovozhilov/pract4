#ifndef EIGHTINTWO_H
#define EIGHTINTWO_H
#include <QString>

QString EightInTwo(QString val);

/**
*\file
*\brief Заголовочный файл функции перевода из 8-ной в 2-ную систему счисления
*\author Alexandr
*\version 1.4
*/

#endif // EIGHTINTWO_H
