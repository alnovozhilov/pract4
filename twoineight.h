#ifndef TWOINEIGHT_H
#define TWOINEIGHT_H
#include <QString>

QString TwoInEight(QString val);

/**
*\file
*\brief Заголовочный файл функции перевода из 2-ной в 8-ную систему счисления
*\author Alexandr
*\version 1.4
*/

#endif // TWOINEIGHT_H
