#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QRegExp>
#include <QValidator>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void SetEnabled();

    void on_twIntn_clicked();

    void on_twInei_clicked();

    void on_eiIntw_clicked();

    void on_eiInsi_clicked();

    void on_tnIntw_clicked();

    void on_siInei_clicked();

private:
    Ui::MainWindow *ui;
    QStandardItemModel *model = new QStandardItemModel;
    QStandardItem *item;
};

/**
*\file
*\brief Заголовочный файл класса главного окна
*\author Alexandr
*\version 1.4
*/

#endif // MAINWINDOW_H
